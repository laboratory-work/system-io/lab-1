MODULE_NAME=llb
DEV_NAME=var2
PATH_SRC=$(PWD)
KERNEL=$(shell uname -r)

obj-m += $(MODULE_NAME).o

all: build install

build:
	/usr/bin/make -C /lib/modules/$(KERNEL)/build M=$(PATH_SRC) modules

clean:
	/usr/bin/make -C /lib/modules/$(KERNEL)/build M=$(PATH_SRC) clean

install:
	sudo insmod $(MODULE_NAME).ko
	lsmod | grep "$(MODULE_NAME)"
	@-sudo chmod 777 /dev/$(DEV_NAME)

uninstall:
	sudo rmmod $(MODULE_NAME).ko
	@-sudo rm -f /dev/$(MODULE_NAME)
	@-sudo rm -f /proc/$(MODULE_NAME)

run:
	@-sudo dmesg -c > /dev/null
	@-sudo rmmod $(MODULE_NAME) 2> /dev/null
	@-sudo rm -f /dev/$(DEV_NAME)
	@-sudo rm -f /proc/$(DEV_NAME)

	sudo insmod $(MODULE_NAME).ko
	sudo dmesg -c
	
	@-sudo chmod 777 /dev/$(DEV_NAME)

	@echo "Драйвер установлен"
	@echo "-----------------"
	@echo "Module  Size  Used by"
	@lsmod | grep "$(MODULE_NAME)"
	@echo "----------------"

	@echo ""
	@echo ">> Run 1:"

	@echo ">>> Write '5+6' to /dev/$(DEV_NAME)"
	printf "5+6" > /dev/$(DEV_NAME)

	@echo ">>> Read result from /proc/$(DEV_NAME)"
	@cat /proc/$(DEV_NAME)