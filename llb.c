#ifndef LLB_1_MODULE
#define LLB_1_MODULE

#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/uaccess.h>
#include <linux/list.h>
#include <linux/slab.h>

#ifndef DEVICE_NAME
	#define DEVICE_NAME "var2"
#endif

#ifndef CLASS_NAME
	#define CLASS_NAME "llb"
#endif

#ifndef SUCCESS
	#define SUCCESS 0
#endif

#ifndef ERRORS_CODE
	#define ERRORS_CODE
	#define NON_ERROR 0
	#define ZERO_DIVISION 1
#endif

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Artem Kiselev");
MODULE_DESCRIPTION("A Linux module for SIO laboratory work #1.");
MODULE_VERSION("0.1");

static struct proc_dir_entry* entry;

static int majorNumber = 0;
static struct device* c_dev;
static struct class* cl;

static int device_open = 0;

struct list_res {
	struct list_head list;
	long result;
	int error;
};

static struct list_head head_res;
static long countOperation;

static int ch_open(struct inode*, struct file*);
static int ch_close(struct inode*, struct file*);
static ssize_t ch_read(struct file*, char __user*, size_t, loff_t*);
static ssize_t ch_write(struct file*, const char __user*, size_t, loff_t*);
static ssize_t proc_read(struct file*, char __user*, size_t, loff_t*);

static struct file_operations ch_ops = {
	.owner = THIS_MODULE,
	.open = ch_open,
	.release = ch_close,
	.read = ch_read,
	.write = ch_write 
};

static struct file_operations proc_fops = {
	.owner = THIS_MODULE,
	.read = proc_read
};

static int ch_open(struct inode* inode, struct file* file) {
	printk(KERN_NOTICE "%s: open()\n", THIS_MODULE->name);

	if (device_open)
		return -EBUSY;

	device_open++;

	try_module_get(THIS_MODULE);
	return SUCCESS;
}

static int ch_close(struct inode* i, struct file* f){
	printk(KERN_INFO "%s: close()\n", THIS_MODULE->name);
	device_open--;

	module_put(THIS_MODULE);
	return SUCCESS;
}

static ssize_t ch_read(struct file* f, char __user* buf, size_t len, loff_t* off){
	printk(KERN_INFO "%s: read()\n", THIS_MODULE->name);
	return len;
}

static ssize_t ch_write(struct file* filep, const char __user* buffer, size_t len, loff_t* offset){
	printk(KERN_INFO "%s: write()\n", THIS_MODULE->name);

	long result;
	int arifmeticError;

	int op1 = 0;
	int op2 = 0;
	int first = 1;
	char op = ' ';

	size_t i;
	size_t start = *offset;
	size_t stop = start + len;

	for (i = start; i < stop; i++) {
		char getChar;
		get_user(getChar, buffer + i);

		if (getChar >= '0' && getChar <= '9') {
			if (first) op1 = op1 * 10 + (getChar - '0');
			else       op2 = op2 * 10 + (getChar - '0');
		}

		if(getChar == '+' || getChar == '-' || getChar == '/' || getChar == '*') {
			if (first) {
				first = 0;
				op = getChar;
			}else {
				len = i;
				break;
			}
		}
	}

	if (op == '+') {
		result = op1 + op2;
	}
	else if (op == '-') {
		result = op1 - op2;
	}
	else if (op == '*') {
		result = op1 * op2;
	}
	else if (op == '/') {
		if (op2 == 0) {
			result = 0;
			arifmeticError = ZERO_DIVISION;
			return len;
		}
		result = op1 / op2;
	}

	arifmeticError = NON_ERROR;
	countOperation++;

	struct list_res *r = kmalloc(sizeof(struct list_res *), GFP_KERNEL);
	r->result = result;
	r->error = arifmeticError;
	list_add(&r->list, &head_res);

	return i;
}

static ssize_t proc_read(struct file* file, char __user *ubuf, size_t count, loff_t* ppos) {
	char *buf = kzalloc(sizeof(char)*25*countOperation, GFP_KERNEL);
	
	struct list_head *ptr;
	struct list_res *entry;
	size_t i = 0;

	list_for_each(ptr, &head_res) {
		entry = list_entry(ptr, struct list_res, list);
		if(entry->error == NON_ERROR) {
			snprintf(buf+(i*25), 25, "%ld\n", entry->result);
			printk(KERN_INFO "%s Result %ld: %ld\n", THIS_MODULE->name, i, entry->result);
		}else if(entry->error == ZERO_DIVISION) {
			snprintf(buf+(i*25), 25, "%s\n", "ERR: ZeroDivision");
			printk(KERN_INFO "%s Result %ld: %s\n", THIS_MODULE->name, i, "ERR: ZeroDivision");
		}
		i++;
	}

	size_t len = 25*countOperation;

	if (*ppos > 0 || count < len){
		return 0;
	}
	if (copy_to_user(ubuf, buf, len) != 0){
		return -EFAULT;
	}

	*ppos = len;

	kfree(buf);

	return len;
}

static int __init initModule(void)
{
	printk(KERN_INFO "Load LabModule1");

	majorNumber = register_chrdev(majorNumber, DEVICE_NAME, &ch_ops);
	if (majorNumber < 0)
	{
		printk(KERN_ALERT "%s: failed to register a major number\n", THIS_MODULE->name);
		return majorNumber;
	}
	printk(KERN_INFO "%s: registered correctly with major number %d\n", THIS_MODULE->name, majorNumber);

	cl = class_create(THIS_MODULE, CLASS_NAME);
	if (IS_ERR(cl)) { 
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "%s: Failed to register device class\n", THIS_MODULE->name);
		return PTR_ERR(cl);
	}
	printk(KERN_INFO "%s: device class registered correctly\n", THIS_MODULE->name);

	c_dev = device_create(cl, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
	if (IS_ERR(c_dev)) {	   
		class_destroy(cl);
		unregister_chrdev(majorNumber, DEVICE_NAME);
		printk(KERN_ALERT "%s: Failed to create the device\n", THIS_MODULE->name);
		return PTR_ERR(c_dev);
	}
	printk(KERN_INFO "%s: device class created correctly\n", THIS_MODULE->name);
	printk(KERN_INFO "%s: /dev/%s is created\n", THIS_MODULE->name, DEVICE_NAME);

	entry = proc_create(DEVICE_NAME, 0444, NULL, &proc_fops);
	printk(KERN_INFO "%s: /proc/%s is created\n", THIS_MODULE->name, DEVICE_NAME);
	
	countOperation = 0;
	INIT_LIST_HEAD(&head_res);

	return SUCCESS;
}

static void __exit exitModule(void)
{
	printk(KERN_INFO "Unload LabModule1");
	
	device_destroy(cl, MKDEV(majorNumber, 0));
	class_unregister(cl);
	class_destroy(cl);
	unregister_chrdev(majorNumber, DEVICE_NAME);
	printk(KERN_INFO "%s: /dev/%s is destroy\n", THIS_MODULE->name, DEVICE_NAME);

	proc_remove(entry);
	printk(KERN_INFO "%s: /proc/%s is destroy\n", THIS_MODULE->name, DEVICE_NAME);

	struct list_head *ptr;
	struct list_res *entry;
	
	list_for_each(ptr, &head_res) {
		entry = list_entry(ptr, struct list_res, list);
		list_del(&entry->list);
	}
}

module_init(initModule);
module_exit(exitModule);

#endif
